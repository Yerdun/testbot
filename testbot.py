import discord
from discord.ext import commands
from calc_align import calculate_alignment
import os

if os.name == "posix": 
    ignore_files_folder = "ignore_files/"
elif os.name == "nt":
    ignore_files_folder = "ignore_files\\"

TOKEN = open(ignore_files_folder + "token.txt").read()
DESCRIPTION = "This bot does whatever I feel like making it do."
INTENTS = discord.Intents.all()
bot = commands.Bot(command_prefix="!", description=DESCRIPTION, intents=INTENTS)


@bot.event
async def on_ready():
    print(f"Logging in: {bot.user} ID: {bot.user.id}")


@bot.command()
async def nick(ctx, member: discord.Member, new_nickname):
    """Changes another user's nickname"""
    await member.edit(nick=new_nickname)
    await ctx.send(f"Changed `{member}`'s nickname to `{new_nickname}`.")

@bot.command()
async def ping(ctx):
    """Responds with pong"""
    await ctx.send(f"Pong")

# !alignment command by Sebastian Clancy and Laurence Garcia
# https://github.com/LaurenceTimothyMGarcia/allignment-bot 
@bot.command()
async def alignment(ctx):
    """Calculates a DND alignment based on the past 100 messages sent in the channel the command was called in"""
    await ctx.channel.send(f"Calculating DND alignment for `{ctx.author}`")
    await ctx.channel.trigger_typing()
    messages = []
    async for my_message in ctx.channel.history(limit=None).filter(
            lambda x:
            x.author.id == ctx.author.id and not x.content.startswith("!")
    ):
        messages.append(my_message)
        if len(messages) >= 100:
            break
    # Debug info, comment out if cluttering up log.
    print("\n!alignment Info:")
    print("Messages: ")
    print([msg.content for msg in messages])
    print("Number of messages:")
    print(len(messages))
    alignment = calculate_alignment([msg.content for msg in messages])
    await ctx.channel.send(f"`{ctx.author}`: {alignment}")

@bot.command()
async def count(ctx, target):
    if ctx.channel.name == "counting":
        async for message in ctx.channel.history(limit = 100):
            if message.content.isdigit():
                lastNumber = int(message.content)
                targetNumber = int(target) + 1
                for x in range(1, targetNumber - lastNumber):
                    await ctx.channel.send(lastNumber + x)
                break
    else:
        print("not in counting")

# Stub for !nick command error, abandoned since I don't know how to do this
#@nick.error
#async def nick_error(ctx, error):
#    if isinstance(error, commands.BadArgument):
#        await ctx.send("You did something wrong. I wish we could know what it was.")


bot.run(TOKEN)